#include "TrigTauHypo/T2CaloTauHypo.h"
#include "TrigTauHypo/T2IDTauHypo.h"
#include "TrigTauHypo/T2IDCoreTauHypo.h"
#include "TrigTauHypo/T2IDIsoTauHypo.h"
#include "TrigTauHypo/T2TauHypo.h"
#include "TrigTauHypo/EFTauInvHypo.h"
#include "TrigTauHypo/EFTauMVHypo.h"
#include "TrigTauHypo/EFTauDiKaonHypo.h"
#include "TrigTauHypo/EFHadCalibHypo.h"
#include "TrigTauHypo/T2TauTauCombHypo.h"
#include "TrigTauHypo/T2TauTauCombFexAlgo.h"
#include "TrigTauHypo/EFTauTauCombHypo.h"
#include "TrigTauHypo/EFTauTauCombFexAlgo.h"
#include "TrigTauHypo/HLTCaloPreSelHypo.h"
#include "TrigTauHypo/HLTTrackPreSelHypo.h"
#include "TrigTauHypo/HLTTauGenericHypo.h"
#include "TrigTauHypo/HLTTauTrackRoiUpdater.h"
#include "TrigTauHypo/HLTTauCaloRoiUpdater.h"
#include "TrigTauHypo/HLTVertexPreSelHypo.h"
#include "TrigTauHypo/HLTVertexCombo.h"
#include "TrigTauHypo/L2TauTopoFex.h"
#include "TrigTauHypo/L2TauTopoHypo.h"
#include "TrigTauHypo/EFTauTopoFex.h"
#include "TrigTauHypo/EFTauTopoHypo.h"
#include "TrigTauHypo/EFPhotonTauFex.h"
#include "TrigTauHypo/EFPhotonTauHypo.h"
#include "../TrigTauCaloRoiUpdaterMT.h"
#include "../TrigTauGenericHypoMT.h"
#include "../TrigTauCaloHypoAlgMT.h"
#include "../TrigTauTrackRoiUpdaterMT.h"
#include "../TrigTrackPreSelHypoAlgMT.h"
#include "../TrigTrackPreSelHypoTool.h"


DECLARE_COMPONENT( T2CaloTauHypo )
DECLARE_COMPONENT( T2IDTauHypo )
DECLARE_COMPONENT( T2IDCoreTauHypo )
DECLARE_COMPONENT( T2IDIsoTauHypo )
DECLARE_COMPONENT( T2TauHypo )
DECLARE_COMPONENT( EFTauInvHypo )
DECLARE_COMPONENT( EFTauMVHypo )
DECLARE_COMPONENT( EFTauDiKaonHypo )
DECLARE_COMPONENT( EFHadCalibHypo )
DECLARE_COMPONENT( T2TauTauCombHypo )
DECLARE_COMPONENT( T2TauTauCombFexAlgo )
DECLARE_COMPONENT( EFTauTauCombHypo )
DECLARE_COMPONENT( EFTauTauCombFexAlgo )
DECLARE_COMPONENT( HLTCaloPreSelHypo )
DECLARE_COMPONENT( HLTTrackPreSelHypo )
DECLARE_COMPONENT( HLTTauGenericHypo )
DECLARE_COMPONENT( HLTTauTrackRoiUpdater )
DECLARE_COMPONENT( HLTTauCaloRoiUpdater )
DECLARE_COMPONENT( HLTVertexPreSelHypo )
DECLARE_COMPONENT( HLTVertexCombo )
DECLARE_COMPONENT( L2TauTopoFex )
DECLARE_COMPONENT( L2TauTopoHypo )
DECLARE_COMPONENT( EFTauTopoFex )
DECLARE_COMPONENT( EFTauTopoHypo )
DECLARE_COMPONENT( EFPhotonTauFex )
DECLARE_COMPONENT( EFPhotonTauHypo )
DECLARE_COMPONENT( TrigTauCaloRoiUpdaterMT )
DECLARE_COMPONENT( TrigTauGenericHypoMT )
DECLARE_COMPONENT( TrigTauCaloHypoAlgMT )
DECLARE_COMPONENT( TrigTauTrackRoiUpdaterMT )
DECLARE_COMPONENT( TrigTrackPreSelHypoAlgMT )
DECLARE_COMPONENT( TrigTrackPreSelHypoTool )
